#!/bin/sh

# Copyright (C) 2024 |Méso|Star> (contact@meso-star.com)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.

set -e

check()
{
  name="$1"
  prog="$2"
  shift 2

  printf "%s " "${name}"
  if ./"${prog}" "$@" > /dev/null 2>&1; then
    printf "\033[1;32mOK\033[m\n"
  else
    printf "\033[1;31mError\033[m\n"
  fi
}

"$@"
