# StArDIS Tests

Set of programs that test the behavior of the
[Stardis](https://gitlab.com/meso-star/stardis.git) program.

## Requirements

- C compiler
- POSIX make
- pkg-config
- [RSys](https://gitlab.com/vaplv/rsys)
- [Star 3D](https://gitlab.com/meso-star/star-3d.git)
- [Star 3DUT](https://gitlab.com/meso-star/star-3dut.git)
- [Star SamPling](https://gitlab.com/meso-star/star-sp.git)
- [Star STL](https://gitlab.com/meso-star/star-stl.git)
- [Stardis](https://gitlab.com/meso-star/stardis.git)

## Run tests

Edit config.mk as needed, then run:

    make test

## License

Copyright (C) 2024 |Méso|Star> (<contact@meso-star.com>). This is
free software released under the GPL v3+ license: GNU GPL version 3 or
later. You are welcome to redistribute it under certain conditions;
refer to the COPYING file for details.
