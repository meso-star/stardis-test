/* Copyright (C) 2024 |Méso|Star> (contact@meso-star.com)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>. */

#include <stardis/stardis-prog-properties.h>

#include <rsys/cstr.h>
#include <rsys/mem_allocator.h>

#include <stdarg.h> /* va_list */
#include <getopt.h>

struct source {
  double position[3];
  double power; /* [W] */
  double diffuse_radiance; /* [W/m^2/sr] */
};
#define SOURCE_NULL__ {{0,0,0}, 0, 0}
static const struct source SOURCE_NULL = SOURCE_NULL__;

/*******************************************************************************
 * Helper functions
 ******************************************************************************/
static void
print_usage(FILE* stream, const char* name)
{
  ASSERT(name);
  fprintf(stream,
    "usage: %s [-h] [-d diffuse_radiance] [-p x,y,z] [-w power]\n",
    name);
}

static res_T
parse_args
  (const struct stardis_description_create_context *ctx,
   struct source* source,
   int argc,
   char* argv[])
{
  size_t len = 0;
  int opt = 0;
  res_T res = RES_OK;
  ASSERT(ctx && source);

  optind = 1;
  while((opt = getopt(argc, argv, "d:hp:w:")) != -1) {
    switch(opt) {
      case 'd':
        res = cstr_to_double(optarg, &source->diffuse_radiance);
        if(res == RES_OK && source->diffuse_radiance < 0) res = RES_BAD_ARG;
        break;
      case 'h':
        print_usage(stdout, ctx->name);
        break;
      case 'p':
        res = cstr_to_list_double(optarg, ',', source->position, &len, 3);
        if(res == RES_OK && len < 3) res = RES_BAD_ARG;
        break;
      case 'w':
        res = cstr_to_double(optarg, &source->power);
        if(res == RES_OK && source->power < 0) res = RES_BAD_ARG;
        break;
      default: res = RES_BAD_ARG; break;
    }
    if(res != RES_OK) {
      if(optarg) {
        fprintf(stderr, "%s: invalid option argument '%s' -- '%c'\n",
          ctx->name, optarg, opt);
      }
      goto error;
    }
  }

exit:
  return res;
error:
  goto exit;
}

/*******************************************************************************
 * Create data
 ******************************************************************************/
void*
stardis_create_data
  (const struct stardis_description_create_context *ctx,
   void* libdata,
   size_t argc,
   char* argv[])
{
  struct source* source = NULL;
  res_T res = RES_OK;
  (void)libdata;

  source = mem_alloc(sizeof(*source));
  if(!source) {
    fprintf(stderr, "%s:%d: error allocating the external spherical source.\n",
      __FILE__, __LINE__);
    goto error;
  }
  *source = SOURCE_NULL;

  res = parse_args(ctx, source, (int)argc, argv);
  if(res != RES_OK) goto error;

exit:
  return source;
error:
  if(source) {
    mem_rm(source);
    source = NULL;
  }
  goto exit;
}

void
stardis_release_data(void* data)
{
  ASSERT(data);
  mem_rm(data);
}

/*******************************************************************************
 * External source
 ******************************************************************************/
double*
stardis_spherical_source_position
  (const double time, /* [s] */
   double position[3],
   void* data)
{
  struct source* source = data;
  (void)time; /* Avoid "unused variable" warning */
  ASSERT(source);
  position[0] = source->position[0];
  position[1] = source->position[1];
  position[2] = source->position[2];
  return position;
}

double /* [W] */
stardis_spherical_source_power(const double time /* [s] */, void* data)
{
  struct source* source = data;
  (void)time; /* Avoid "unused variable" warning */
  ASSERT(source);
  return source->power; /* [W] */
}

double /* [W/m^2/sr] */
stardis_spherical_source_diffuse_radiance
  (const double time, /* [s] */
   const double dir[3],
   void* data)
{
  struct source* source = data;
  (void)time, (void)dir; /* Avoid "unused variable" warning */
  ASSERT(source);
  return source->diffuse_radiance;
}

/*******************************************************************************
 * Legal notices
 ******************************************************************************/
const char*
get_copyright_notice(void* data)
{
  (void)data; /* Avoid "unused variable" warnings */
  return "Copyright (C) 2024 |Méso|Star> (contact@meso-star.com)";
}

const char*
get_license_short(void* data)
{
  (void)data; /* Avoid "unused variable" warnings */
  return "GNU GPL version 3 or later <http://www.gnu.org/licenses/>";
}

const char*
get_license_text(void* data)
{
  (void)data; /* Avoid "unused variable" warnings */
  return
    "This is free software released under the GPL v3+ license: GNU GPL\n"
    "version 3 or later. You are welcome to redistribute it under certain\n"
    "conditions; refer to <http://www.gnu.org/licenses/> for details.";
}
