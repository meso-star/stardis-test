/* Copyright (C) 2024 |Méso|Star> (contact@meso-star.com)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>. */

#include "sadist.h"

#include <stardis/stardis-prog-properties.h>

#include <rsys/cstr.h>
#include <rsys/rsys.h>
#include <rsys/mem_allocator.h>

#include <getopt.h>

/*******************************************************************************
 * Helper functions
 ******************************************************************************/
static void
print_usage(FILE* stream, const char* name)
{
  fprintf(stream,
"usage: %s [-h] [-k kx,ky,kz] [-m lambda,rho,cp] [-p A,B1,B2]\n", name);
}

static res_T
parse_periodicities(struct sadist_unsteady_profile* profile, const char* str)
{
  double vec[3] = {0, 0, 0};
  size_t len = 0;
  res_T res = RES_OK;
  ASSERT(profile && str);

  res = cstr_to_list_double(str, ',', vec, &len, 3);
  if(res == RES_OK && len != 3) res = RES_BAD_ARG;
  if(res != RES_OK) {
    fprintf(stderr,
      "%s:%d: unable to parse the periodicity parmeters `%s' -- %s\n",
      __FILE__, __LINE__, str, res_to_cstr(res));
    goto error;
  }

  profile->kx = vec[0];
  profile->ky = vec[1];
  profile->kz = vec[2];

exit:
  return res;
error:
  goto exit;
}

static res_T
parse_medium_properties
  (struct sadist_unsteady_profile* profile,
   const char* str)
{
  double vec[3] = {0, 0, 0};
  size_t len = 0;
  res_T res = RES_OK;
  ASSERT(profile && str);

  res = cstr_to_list_double(str, ',', vec, &len, 3);
  if(res == RES_OK && len != 3) res = RES_BAD_ARG;
  if(res != RES_OK) {
    fprintf(stderr,
      "%s:%d: unable to parse the medium properties `%s' -- %s\n",
      __FILE__, __LINE__, str, res_to_cstr(res));
    goto error;
  }

  if(vec[0] <= 0) { /* Lambda */
    fprintf(stderr,
      "%s:%d: invalid thermal conductivity %g. It cannot be negative or null\n",
      __FILE__, __LINE__, vec[0]);
    res = RES_BAD_ARG;
    goto error;
  }

  if(vec[1] <= 0) { /* rho */
    fprintf(stderr,
      "%s:%d: invalid volumic mass %g. It cannot be negative or null\n",
      __FILE__, __LINE__, vec[1]);
    res = RES_BAD_ARG;
    goto error;
  }

  if(vec[2] <= 0) { /* cp */
    fprintf(stderr,
      "%s:%d: invalid calorific capacity%g. It cannot be negative or null\n",
      __FILE__, __LINE__, vec[2]);
    res = RES_BAD_ARG;
    goto error;
  }

  profile->lambda = vec[0];
  profile->rho = vec[1];
  profile->cp = vec[2];

exit:
  return res;
error:
  goto exit;
}

static res_T
parse_scale_factors
  (struct sadist_unsteady_profile* profile,
   const char* str)
{
  double vec[3] = {0, 0, 0};
  size_t len = 0;
  res_T res = RES_OK;
  ASSERT(profile && str);

  res = cstr_to_list_double(str, ',', vec, &len, 3);
  if(res == RES_OK && len != 3) res = RES_BAD_ARG;
  if(res != RES_OK) {
    fprintf(stderr,
      "%s:%d: unable to parse the scale factors `%s' -- %s\n",
      __FILE__, __LINE__, str, res_to_cstr(res));
    goto error;
  }

  if(vec[0] < 0 || vec[1] < 0 || vec[2] < 0) {
    fprintf(stderr,
      "%s:%d: invalid scale factors `%s'. They cannot be negative\n",
      __FILE__, __LINE__, str);
    res = RES_BAD_ARG;
    goto error;
  }

  profile->A = vec[0];
  profile->B1 = vec[1];
  profile->B2 = vec[2];

exit:
  return res;
error:
  goto exit;
}

static res_T
parse_args
  (const struct stardis_description_create_context* ctx,
   struct sadist_unsteady_profile* profile,
   int argc,
   char* argv[])
{
  int opt;
  res_T res = RES_OK;
  ASSERT(ctx && profile);

  optind = 1;
  while((opt = getopt(argc, argv, "hk:m:p:")) != -1) {
    switch(opt) {
      case 'h':
        print_usage(stdout, ctx->name);
        break;
      case 'k':
        res = parse_periodicities(profile, optarg);
        if(res != RES_OK) goto error;
        break;
      case 'm':
        res = parse_medium_properties(profile, optarg);
        if(res != RES_OK) goto error;
        break;
      case 'p':
        res = parse_scale_factors(profile, optarg);
        if(res != RES_OK) goto error;
        break;
      default: res = RES_BAD_ARG; break;
    }
  }

exit:
  return res;
error:
  goto exit;
}

/*******************************************************************************
 * Legal notices
 ******************************************************************************/
const char*
get_copyright_notice(void* data)
{
  (void)data; /* Avoid "unused variable" warnings */
  return "Copyright (C) 2024 |Méso|Star> (contact@meso-star.com)";
}

const char*
get_license_short(void* data)
{
  (void)data; /* Avoid "unused variable" warnings */
  return "GNU GPL version 3 or later <http://www.gnu.org/licenses/>";
}

const char*
get_license_text(void* data)
{
  (void)data; /* Avoid "unused variable" warnings */
  return
    "This is free software released under the GPL v3+ license: GNU GPL\n"
    "version 3 or later. You are welcome to redistribute it under certain\n"
    "conditions; refer to <http://www.gnu.org/licenses/> for details.";
}


/*******************************************************************************
 * Create data
 ******************************************************************************/
void*
stardis_create_data
  (const struct stardis_description_create_context* ctx,
   void* libdata,
   size_t argc,
   char* argv[])
{
  struct sadist_unsteady_profile* profile = NULL;
  res_T res = RES_OK;
  (void)libdata;

  profile = mem_alloc(sizeof(*profile));
  if(!profile) {
    fprintf(stderr, "%s:%d: error allocating the unsteady profile.\n",
      __FILE__, __LINE__);
    goto error;
  }

  *profile = SADIST_UNSTEADY_PROFILE_NULL;

  res = parse_args(ctx, profile, (int)argc, argv);
  if(res != RES_OK) goto error;

exit:
  return profile;
error:
  if(profile) {
    mem_rm(profile);
    profile = NULL;
  }
  goto exit;
}

void
stardis_release_data(void* data)
{
  ASSERT(data);
  mem_rm(data);
}

/*******************************************************************************
 * Boundary condition
 ******************************************************************************/
double
stardis_boundary_temperature
  (const struct stardis_interface_fragment* frag,
   void* data)
{
  ASSERT(frag && data);
  return sadist_unsteady_profile_temperature(data, frag->P, frag->time);
}

double*
stardis_t_range(void* data, double range[2])
{
  ASSERT(data && range);
  (void)data;
  range[0] = 0;
  range[1] = INF;
  return range;
}
