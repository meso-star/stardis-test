/* Copyright (C) 2024 |Méso|Star> (contact@meso-star.com)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>. */

#define _POSIX_C_SOURCE 200112L /* popen */

#include "sadist.h"

#include <star/s3dut.h>

#include <rsys/math.h>
#include <rsys/mem_allocator.h>

#include <errno.h>
#include <stdio.h> /* popen */
#include <string.h> /* strerror */
#include <wait.h> /* WIFEXITED, WEXITSTATUS */

/*
 * The system is an unsteady-state temperature profile, meaning that at any
 * point, at any time, we can analytically calculate the temperature. We
 * immerse in this temperature field a supershape representing a solid in which
 * we want to evaluate the temperature by Monte Carlo at a given position and
 * observation time. On the Monte Carlo side, the temperature of the supershape
 * is unknown. Only the boundary temperature is fixed at the temperature of the
 * unsteady trilinear profile mentioned above. Monte Carlo would then have to
 * find the temperature defined by the unsteady profile.
 *
 *      T(z)             /\ <-- T(x,y,z,t)
 *       |  T(y)     ___/  \___
 *       |/          \  . T=? /
 *       o--- T(x)   /_  __  _\
 *                     \/  \/
 */

#define FILENAME_SSHAPE "sshape.stl"
#define FILENAME_SCENE "scene.txt"

#define LAMBDA 0.1  /* [W/(m.K)] */
#define RHO 25.0 /* [kg/m^3] */
#define CP 2.0 /* [J/K/kg)] */
#define DELTA 1.0/20.0 /* [m/fp_to_meter] */

/* Parameters of the unsteady profile */
#define UPROF_A 0.0
#define UPROF_B1 10.0
#define UPROF_B2 1000.0
#define UPROF_K (PI/4.0)

/* Probe position */
#define PX 0.2 /* [m/fp_to_meter] */
#define PY 0.3 /* [m/fp_to_meter] */
#define PZ 0.4 /* [m/fp_to_meter] */

/* Observation time */
#define TIME 5 /* [s] */

#define NREALISATIONS 100000

#define COMMAND "stardis -V3 -p "STR(PX)","STR(PY)","STR(PZ)","STR(TIME) \
  " -n "STR(NREALISATIONS)" -M "FILENAME_SCENE " -I -INF"
#define COMMAND_DSPHERE COMMAND" -a dsphere"
#define COMMAND_WOS COMMAND" -a wos"

/*******************************************************************************
 * Helper functions
 ******************************************************************************/
static void
setup_sshape(FILE* stream)
{
  struct s3dut_mesh* sshape = NULL;
  struct s3dut_mesh_data sshape_data;
  struct s3dut_super_formula f0 = S3DUT_SUPER_FORMULA_NULL;
  struct s3dut_super_formula f1 = S3DUT_SUPER_FORMULA_NULL;
  const double radius = 1;
  const unsigned nslices = 256;

  f0.A = 1.5; f0.B = 1; f0.M = 11.0; f0.N0 = 1; f0.N1 = 1; f0.N2 = 2.0;
  f1.A = 1.0; f1.B = 2; f1.M =  3.6; f1.N0 = 1; f1.N1 = 2; f1.N2 = 0.7;
  S3DUT(create_super_shape(NULL, &f0, &f1, radius, nslices, nslices/2, &sshape));
  S3DUT(mesh_get_data(sshape, &sshape_data));

  sadist_write_stl(stream, sshape_data.positions, sshape_data.nvertices,
    sshape_data.indices, sshape_data.nprimitives);

  S3DUT(mesh_ref_put(sshape));
}

static void
setup_scene
  (FILE* fp,
   const char* sshape,
   struct sadist_unsteady_profile* profile)
{
  ASSERT(sshape && profile);

  fprintf(fp, "PROGRAM unsteady_profile libsadist_unsteady_profile.so\n");
  fprintf(fp, "SOLID SuperShape %g %g %g %g 0 UNKNOWN 0 BACK %s\n",
    LAMBDA, RHO, CP, DELTA, sshape);

  fprintf(fp, "T_BOUNDARY_FOR_SOLID_PROG Dirichlet unsteady_profile %s", sshape);
  fprintf(fp, " PROG_PARAMS -p %g,%g,%g -k %g,%g,%g -m %g,%g,%g\n",
   UPROF_A, UPROF_B1, UPROF_B2, UPROF_K, UPROF_K, UPROF_K, LAMBDA, RHO, CP);

  profile->A = UPROF_A;
  profile->B1 = UPROF_B1;
  profile->B2 = UPROF_B2;
  profile->kx = UPROF_K;
  profile->ky = UPROF_K;
  profile->kz = UPROF_K;
  profile->lambda = LAMBDA;
  profile->rho = RHO;
  profile->cp = CP;
}

static int
init(struct sadist_unsteady_profile* profile)
{
  FILE* fp_sshape = NULL;
  FILE* fp_scene = NULL;
  int err = 0;

  if((fp_sshape = fopen(FILENAME_SSHAPE, "w")) == NULL) {
    fprintf(stderr, "Error opening the `"FILENAME_SSHAPE"' file -- %s\n",
      strerror(errno));
    err = errno;
    goto error;
  }

  if((fp_scene = fopen(FILENAME_SCENE, "w")) == NULL) {
    fprintf(stderr, "Error opening the `"FILENAME_SCENE"' file -- %s\n",
      strerror(errno));
    err = errno;
    goto error;
  }

  setup_sshape(fp_sshape);
  setup_scene(fp_scene, FILENAME_SSHAPE, profile);

exit:
  if(fp_sshape && fclose(fp_sshape)) { perror("fclose"); if(!err) err = errno; }
  if(fp_scene && fclose(fp_scene)) { perror("fclose"); if(!err) err = errno; }
  return err;
error:
  goto exit;
}

static int
run(struct sadist_unsteady_profile* profile, const char* command)
{
  const double P[3] = {PX, PY, PZ};
  FILE* output = NULL;
  double ref = 0;
  double E = 0;
  double SE = 0;
  int n = 0;
  int err = 0;
  int status = 0;

  printf("%s\n", command);

  if(!(output = popen(command, "r"))) {
    fprintf(stderr, "Error executing stardis -- %s\n", strerror(errno));
    fprintf(stderr, "\t"COMMAND"\n");
    err = errno;
    goto error;
  }

  if((n = fscanf(output, "%lf %lf %*d %*d", &E, &SE)), n != 2 && n != EOF) {
    fprintf(stderr, "Error reading the output stream -- %s\n", strerror(errno));
    err = errno;
    goto error;
  }

  /* Check command exit status */
  if((status=pclose(output), output=NULL, status)) {
    if(status == -1) err = errno;
    else if(WIFEXITED(status)) err = WEXITSTATUS(status);
    else if(WIFSIGNALED(status)) err = WTERMSIG(status);
    else if(WIFSTOPPED(status)) err = WSTOPSIG(status);
    else FATAL("Unreachable code.\n");
    goto error;
  }

  ref = sadist_unsteady_profile_temperature(profile, P, TIME);
  printf("T = %g ~ %g +/- %g\n", ref, E, SE);
  if(!eq_eps(ref, E, SE*3)) {
    err = 1;
    goto error;
  }

exit:
  if(output) pclose(output);
  return err;
error:
  goto exit;
}

/*******************************************************************************
 * The test
 ******************************************************************************/
int
main(int argc, char** argv)
{
  struct sadist_unsteady_profile profile = SADIST_UNSTEADY_PROFILE_NULL;
  int err = 0;
  (void)argc, (void)argv;

  if((err = init(&profile))) goto error;
  if((err = run(&profile, COMMAND_DSPHERE))) goto error;
  if((err = run(&profile, COMMAND_WOS))) goto error;

exit:
  if(mem_allocated_size() != 0) {
    fprintf(stderr, "Memory leaks: %lu bytes\n", mem_allocated_size());
    if(!err) err = -1;
  }
  return err;
error:
  goto exit;
}
