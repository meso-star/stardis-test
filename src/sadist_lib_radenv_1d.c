/* Copyright (C) 2024 |Méso|Star> (contact@meso-star.com)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>. */

#include <stardis/stardis-prog-properties.h>

#include <rsys/cstr.h>
#include <rsys/mem_allocator.h>

#include <getopt.h>

/* Define a radiative environment whose temperature depend on the X direction */
struct radenv {
  double temperatures[2]; /* [K] */
  double reference_temperatures[2]; /* [K] */
};
#define RADENV_NULL__ {{0,0}, {0,0}}
static const struct radenv RADENV_NULL = RADENV_NULL__;

/*******************************************************************************
 * Helper functions
 ******************************************************************************/
static void
print_usage(FILE* stream, const char* name)
{
  ASSERT(name);
  fprintf(stream, "usage: %s [-h] [-r t0,t1] [-t t0,t1]\n", name);
}

static res_T
parse_temps(const char* str, double temps[2])
{
  size_t len = 0;
  res_T res = RES_OK;
  ASSERT(str && temps);

  res = cstr_to_list_double(str, ',', temps, &len, 2);
  if(res != RES_OK && len != 2) res = RES_BAD_ARG;
  if(res != RES_OK) goto error;

exit:
  return res;
error:
  goto exit;
}

static res_T
parse_trefs(const char* str, double trefs[2])
{
  size_t len = 0;
  res_T res = RES_OK;
  ASSERT(str && trefs);

  res = cstr_to_list_double(str, ',', trefs, &len, 2);
  if(res != RES_OK && len != 2) res = RES_BAD_ARG;
  if(res != RES_OK) goto error;

  if(trefs[0] < 0 || trefs[1] < 0) {
    fprintf(stderr, "A reference temperature cannot be negative.\n");
    res = RES_BAD_ARG;
    goto error;
  }

exit:
  return res;
error:
  goto exit;
}

static res_T
parse_args
  (const struct stardis_description_create_context* ctx,
   struct radenv* radenv,
   const int argc,
   char* argv[])
{
  int opt = 0;
  res_T res = RES_OK;

  /* Check pre-conditions */
  ASSERT(ctx && radenv);

  optind = 1;
  while((opt = getopt(argc, argv, "ht:r:")) != -1) {
    switch(opt) {
      case 'h':
        print_usage(stdout, ctx->name);
        break;
      case 'r':
        res = parse_trefs(optarg, radenv->reference_temperatures);
        break;
      case 't':
        res = parse_temps(optarg, radenv->temperatures);
        break;
      default: res = RES_BAD_ARG; break;
    }
    if(res != RES_OK) {
      if(optarg) {
        fprintf(stderr, "%s: invalid option argument '%s' -- '%c'\n",
          ctx->name, optarg, opt);
      }
      goto error;
    }
  }

exit:
  return res;
error:
  goto exit;
}

/*******************************************************************************
 * Create data
 ******************************************************************************/
void*
stardis_create_data
  (const struct stardis_description_create_context *ctx,
   void* libdata,
   size_t argc,
   char* argv[])
{
  struct radenv* radenv = NULL;
  res_T res = RES_OK;
  (void)libdata;

  radenv = mem_alloc(sizeof(*radenv));
  if(!radenv) {
    fprintf(stderr, "%s:%d: error allocating the radiative environment.\n",
      __FILE__, __LINE__);
    goto error;
  }
  *radenv = RADENV_NULL;

  res = parse_args(ctx, radenv, (int)argc, argv);
  if(res != RES_OK) goto error;

exit:
  return radenv;
error:
  if(radenv) {
    mem_rm(radenv);
    radenv = NULL;
  }
  goto exit;
}

void
stardis_release_data(void* data)
{
  ASSERT(data);
  mem_rm(data);
}

/*******************************************************************************
 * Radiative environment
 ******************************************************************************/
double /* [K] */
stardis_radiative_env_temperature
  (const double time, /* [s] */
   const double dir[3],
   void* data)
{
  struct radenv* radenv = data;
  (void)time;
  ASSERT(radenv);
  return radenv->temperatures[dir[0] > 0]; /* [K] */
}

double /* [K] */
stardis_radiative_env_reference_temperature
  (const double time, /* [s] */
   const double dir[3],
   void* data)
{
  struct radenv* radenv = data;
  (void)time;
  ASSERT(radenv);
  return radenv->reference_temperatures[dir[0] > 0]; /* [K] */
}

double*
stardis_t_range(void* data, double t_range[2])
{
  struct radenv* radenv = data;
  const double* trefs = NULL;
  ASSERT(radenv);

  trefs = radenv->reference_temperatures;
  t_range[0] = MMIN(trefs[0], trefs[1]);
  t_range[1] = MMAX(trefs[0], trefs[1]);
  return t_range;
}

/*******************************************************************************
 * Legal notices
 ******************************************************************************/
const char*
get_copyright_notice(void* data)
{
  (void)data; /* Avoid "unused variable" warnings */
  return "Copyright (C) 2024 |Méso|Star> (contact@meso-star.com)";
}

const char*
get_license_short(void* data)
{
  (void)data; /* Avoid "unused variable" warnings */
  return "GNU GPL version 3 or later <http://www.gnu.org/licenses/>";
}

const char*
get_license_text(void* data)
{
  (void)data; /* Avoid "unused variable" warnings */
  return
    "This is free software released under the GPL v3+ license: GNU GPL\n"
    "version 3 or later. You are welcome to redistribute it under certain\n"
    "conditions; refer to <http://www.gnu.org/licenses/> for details.";
}
