/* Copyright (C) 2024 |Méso|Star> (contact@meso-star.com)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>. */

#ifndef SADIST_H
#define SADIST_H

#include <rsys/rsys.h>
#include <math.h>
#include <errno.h> /* EINVAL */

/*******************************************************************************
 * Trilinear profile
 ******************************************************************************/
struct sadist_trilinear_profile {
  /* Spatial range in which the trilinear profile is defined */
  double lower[3];
  double upper[3];

  double a[2]; /* Interpolated values along X */
  double b[2]; /* Interpolated values along Y */
  double c[2]; /* Interpolated values along Z */
};
#define SADIST_TRILINEAR_PROFILE_NULL__ {{0,0,0}, {0,0,0}, {0,0}, {0,0}, {0,0}}
static const struct sadist_trilinear_profile SADIST_TRILINEAR_PROFILE_NULL =
  SADIST_TRILINEAR_PROFILE_NULL__;

static INLINE int
sadist_trilinear_profile_check(const struct sadist_trilinear_profile* profile)
{
  ASSERT(profile);
  if(profile->lower[0] >= profile->upper[0]
  || profile->lower[1] >= profile->upper[1]
  || profile->lower[2] >= profile->upper[2])
    return EINVAL;

  return 0;
}

static INLINE double
sadist_trilinear_profile_temperature
  (const struct sadist_trilinear_profile* profile,
   const double p[3])
{
  double u, v, w;
  ASSERT(profile && p);
  u = (p[0] - profile->lower[0]) / (profile->upper[0] - profile->lower[0]);
  v = (p[1] - profile->lower[1]) / (profile->upper[1] - profile->lower[1]);
  w = (p[2] - profile->lower[2]) / (profile->upper[2] - profile->lower[2]);
  return u * (profile->a[1] - profile->a[0]) + profile->a[0]
       + v * (profile->b[1] - profile->b[0]) + profile->b[0]
       + w * (profile->c[1] - profile->c[0]) + profile->c[0];
}

/*******************************************************************************
 * Unsteady profile
 ******************************************************************************/
struct sadist_unsteady_profile {
  double A; /* Influence of the thermal source */
  double B1; /* Influence of space variation */
  double B2; /* Influence of spatio-temporal variations */

  double kx; /* Spatio-temporal periodicity in X */
  double ky; /* Spatio-temporal periodicity in Y */
  double kz; /* Spatio-temporal periodicity in Z */

  double lambda; /* Thermal conductivity [W/m/K] */
  double rho; /* Volumic mass [kg/m^3] */
  double cp; /* Calorific capacity [J/K/kg] */
};
#define SADIST_UNSTEADY_PROFILE_NULL__ {0,0,0,0,0,0,0,0,0}
static const struct sadist_unsteady_profile SADIST_UNSTEADY_PROFILE_NULL =
  SADIST_UNSTEADY_PROFILE_NULL__;

static INLINE double
sadist_unsteady_profile_temperature
  (const struct sadist_unsteady_profile* profile,
   const double pos[3],
   const double time)
{
  double alpha; /* Diffusivity */
  double kx, ky, kz;
  double x, y, z, t;
  double a, b, c;
  double temp;

  ASSERT(profile && pos);

  x = pos[0];
  y = pos[1];
  z = pos[2];
  t = time;

  alpha = profile->lambda / (profile->rho*profile->cp);

  kx = profile->kx;
  ky = profile->ky;
  kz = profile->kz;

  a = (x*x*x*z-3*x*y*y*z);
  b = sin(kx*x)*sin(ky*y)*sin(kz*z)*exp(-alpha*(kx*kx + ky*ky + kz*kz)*t);
  c = x*x*x*x * y*y*y * z*z;

  temp = (profile->B1*a + profile->B2*b - profile->A*c) / profile->lambda;
  return temp;
}

static INLINE double
sadist_unsteady_profile_power
  (const struct sadist_unsteady_profile* profile,
   const double pos[3])
{
  double x, y, z;
  double p;
  ASSERT(profile && pos);

  x = pos[0];
  y = pos[1];
  z = pos[2];

  p = 12*x*x*y*y*y*z*z + 6*x*x*x*x*y*z*z + 2*x*x*x*x*y*y*y;
  p = profile->A * p;
  return p;
}

/*******************************************************************************
 * Miscellaneous
 ******************************************************************************/
static INLINE void
sadist_write_stl
  (FILE* stream,
   const double* positions,
   const size_t nvertices,
   const size_t* indices,
   const size_t ntriangles)
{
  size_t itri;
  (void)nvertices;

  fprintf(stream, "solid ascii\n");
  FOR_EACH(itri, 0, ntriangles) {
    const double* v0 = positions + indices[itri*3+0]*3;
    const double* v1 = positions + indices[itri*3+1]*3;
    const double* v2 = positions + indices[itri*3+2]*3;
    fprintf(stream, "facet normal 0 1 0\n");
    fprintf(stream, "\touter loop\n");
    fprintf(stream, "\t\tvertex %g %g %g\n", SPLIT3(v0));
    fprintf(stream, "\t\tvertex %g %g %g\n", SPLIT3(v1));
    fprintf(stream, "\t\tvertex %g %g %g\n", SPLIT3(v2));
    fprintf(stream, "\tendloop\n");
    fprintf(stream, "endfacet\n");
  }
  fprintf(stream, "endsolid\n");
}

#endif /* SADIST_H */
