/* Copyright (C) 2024 |Méso|Star> (contact@meso-star.com)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>. */

#include "sadist.h"

#include <stardis/stardis-prog-properties.h>

#include <rsys/cstr.h>
#include <rsys/rsys.h>
#include <rsys/mem_allocator.h>

#include <stdarg.h> /* va_list */
#include <getopt.h>

/*******************************************************************************
 * Helper functions
 ******************************************************************************/
static void
log_err(const char* msg, ...)
{
  va_list vargs_list;
  ASSERT(msg);

  va_start(vargs_list, msg);
  vfprintf(stderr, msg, vargs_list);
  va_end(vargs_list);
}

static void
print_usage(FILE* stream, const char* name)
{
  fprintf(stream,
"usage: %s [-h] [-b lower_xyz,upper_xyz]\n"
"   [-t max_Tx,max_Ty,max_Tz]\n",
  name);
}

static res_T
parse_spatial_range(struct sadist_trilinear_profile* profile, const char* str)
{
  double range[2] = {0, 0};
  size_t len = 0;
  res_T res = RES_OK;
  ASSERT(profile && str);

  res = cstr_to_list_double(str, ',', range, &len, 2);
  if(res == RES_OK && len != 2) res = RES_BAD_ARG;
  if(res != RES_OK) {
    log_err("%s:%lu: unable to parse spatial range parameter `%s' -- %s\n",
      __FILE__, __LINE__, str, res_to_cstr(res));
    goto error;
  }

  if(range[0] >= range[1]) {
    log_err("%s:%lu: invalid spatial range [%g, %g]\n",
      __FILE__, __LINE__, range[0], range[1]);
    res = RES_BAD_ARG;
    goto error;
  }

  profile->lower[0] = profile->lower[1] = profile->lower[2] = range[0];
  profile->upper[0] = profile->upper[1] = profile->upper[2] = range[1];

exit:
  return res;
error:
  goto exit;
}

static res_T
parse_max_temperatures(struct sadist_trilinear_profile* profile, const char* str)
{
  double abc[3] = {0, 0, 0};
  size_t len = 0;
  res_T res = RES_OK;
  ASSERT(profile && str);

  res = cstr_to_list_double(str, ',', abc, &len, 3);
  if(res == RES_OK && len != 3) res = RES_BAD_ARG;
  if(res != RES_OK) {
    log_err("%s:%lu: unable to parse maximum temperatures `%s' -- %s\n",
      __FILE__, __LINE__, str, res_to_cstr(res));
    goto error;
  }

  if(abc[0] < 0 || abc[1] < 0 || abc[2] < 0) {
    log_err
      ("%s:%lu: invalid maximum temperatures (X = %g K, Y = %g K, Z = %g K)\n",
       __FILE__, __LINE__, str, abc[0], abc[1], abc[2]);
    res = RES_BAD_ARG;
    goto error;
  }

  profile->a[1] = abc[0];
  profile->b[1] = abc[1];
  profile->c[1] = abc[2];

exit:
  return res;
error:
  goto exit;
}

static res_T
parse_args
  (const struct stardis_description_create_context* ctx,
   struct sadist_trilinear_profile* profile,
   int argc,
   char* argv[])
{
  int opt;
  res_T res = RES_OK;
  ASSERT(ctx && profile);

  optind = 1;
  while((opt = getopt(argc, argv, "b:ht:")) != -1) {
    switch(opt) {
      case 'b':
        res = parse_spatial_range(profile, optarg);
        if(res != RES_OK) goto error;
        break;
      case 'h':
        print_usage(stdout, ctx->name);
        break;
      case 't':
        res = parse_max_temperatures(profile, optarg);
        if(res != RES_OK) goto error;
        break;
      default: res = RES_BAD_ARG; break;
    }
  }

exit:
  return res;
error:
  goto exit;
}

/*******************************************************************************
 * Legal notices
 ******************************************************************************/
const char*
get_copyright_notice(void* data)
{
  (void)data; /* Avoid "unused variable" warnings */
  return "Copyright (C) 2024 |Méso|Star> (contact@meso-star.com)";
}

const char*
get_license_short(void* data)
{
  (void)data; /* Avoid "unused variable" warnings */
  return "GNU GPL version 3 or later <http://www.gnu.org/licenses/>";
}

const char*
get_license_text(void* data)
{
  (void)data; /* Avoid "unused variable" warnings */
  return
    "This is free software released under the GPL v3+ license: GNU GPL\n"
    "version 3 or later. You are welcome to redistribute it under certain\n"
    "conditions; refer to <http://www.gnu.org/licenses/> for details.";
}

/*******************************************************************************
 * Create data
 ******************************************************************************/
void*
stardis_create_data
  (const struct stardis_description_create_context* ctx,
   void* libdata,
   size_t argc,
   char* argv[])
{
  struct sadist_trilinear_profile* profile = NULL;
  res_T res = RES_OK;
  (void)libdata;

  profile = mem_alloc(sizeof(*profile));
  if(!profile) {
    log_err("%s:%lu: error allocating the trilinear profile.\n",
      __FILE__, __LINE__);
    goto error;
  }

  *profile = SADIST_TRILINEAR_PROFILE_NULL;

  res = parse_args(ctx, profile, (int)argc, argv);
  if(res != RES_OK) goto error;

exit:
  return profile;
error:
  if(profile) {
    mem_rm(profile);
    profile = NULL;
  }
  goto exit;
}

void
stardis_release_data(void* data)
{
  ASSERT(data);
  mem_rm(data);
}

/*******************************************************************************
 * Boundary condition
 ******************************************************************************/
double
stardis_boundary_temperature
  (const struct stardis_interface_fragment* frag,
   void* data)
{
  ASSERT(frag && data);
  return sadist_trilinear_profile_temperature(data, frag->P);
}

double*
stardis_t_range(void* data, double range[2])
{
  struct sadist_trilinear_profile* profile = data;
  ASSERT(data && range);

  range[0] =      MMIN(profile->a[0], profile->a[1]);
  range[0] = MMIN(MMIN(profile->b[0], profile->b[1]), range[0]);
  range[0] = MMIN(MMIN(profile->c[0], profile->c[1]), range[0]);

  range[1] =      MMAX(profile->a[0], profile->a[1]);
  range[1] = MMAX(MMAX(profile->b[0], profile->b[1]), range[1]);
  range[1] = MMAX(MMAX(profile->c[0], profile->c[1]), range[1]);

  return range;
}
