/* Copyright (C) 2024 |Méso|Star> (contact@meso-star.com)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>. */

#include <stardis/stardis-prog-properties.h>

#include <rsys/cstr.h>
#include <rsys/mem_allocator.h>

#include <getopt.h>

/* Define an interface between a solid and a fluid */
struct solid_fluid {
  double emissivity;
  double specular_fraction;
  double convection_coef; /* [W/K/m^2] */
  double tref; /* Reference temperature [K] */
};
#define SOLID_FLUID_NULL__ {0,0,0,0}
static const struct solid_fluid SOLID_FLUID_NULL = SOLID_FLUID_NULL__;

/******************************************************************************
 * Helper functions
 ******************************************************************************/
static void
print_usage(FILE* stream, const char* name)
{
  ASSERT(name);
  fprintf(stream, "usage: %s [-h] [-c convection_coef] [-e emissivity]\n"
    "\t[-r reference_temperature] [-s specular_fraction]\n", name);
}

static res_T
parse_args
  (const struct stardis_description_create_context* ctx,
   struct solid_fluid* sf,
   const int argc,
   char* argv[])
{
  int opt = 0;
  res_T res = RES_OK;

  /* Check pre-conditions */
  ASSERT(ctx && sf);

  optind = 1;
  while((opt = getopt(argc, argv, "c:e:hr:s:")) != -1) {
    switch(opt) {
      case 'c':
        res = cstr_to_double(optarg, &sf->convection_coef);
        if(res == RES_OK && sf->convection_coef < 0) res = RES_BAD_ARG;
        break;
      case 'e':
        res = cstr_to_double(optarg, &sf->emissivity);
        if(res == RES_OK && (sf->emissivity < 0 || sf->emissivity > 1))
          res = RES_BAD_ARG;
        break;
      case 'h':
        print_usage(stdout, ctx->name);
        break;
      case 'r':
        res = cstr_to_double(optarg, &sf->tref);
        if(res != RES_OK && sf->tref < 0) res = RES_BAD_ARG;
        break;
      case 's':
        res = cstr_to_double(optarg, &sf->specular_fraction);
        if(res == RES_OK
        && (sf->specular_fraction < 0 || sf->specular_fraction > 1)) {
          res = RES_BAD_ARG;
        }
        break;
      default: res = RES_BAD_ARG; break;
    }
    if(res != RES_OK) {
      if(optarg) {
        fprintf(stderr, "%s: invalid option argument '%s' -- '%c'\n",
          ctx->name, optarg, opt);
      }
      goto error;
    }
  }
exit:
  return res;
error:
  goto exit;
}

/******************************************************************************
 * Create data
 ******************************************************************************/
void*
stardis_create_data
  (const struct stardis_description_create_context *ctx,
   void* libdata,
   size_t argc,
   char* argv[])
{
  struct solid_fluid* solid_fluid = NULL;
  res_T res = RES_OK;
  (void)libdata;

  solid_fluid = mem_alloc(sizeof(*solid_fluid));
  if(!solid_fluid) {
    fprintf(stderr, "%s:%d: error allocating the solid fluid connection.\n",
      __FILE__, __LINE__);
    goto error;
  }
  *solid_fluid = SOLID_FLUID_NULL;

  res = parse_args(ctx, solid_fluid, (int)argc, argv);
  if(res != RES_OK) goto error;

exit:
  return solid_fluid;
error:
  if(solid_fluid) {
    mem_rm(solid_fluid);
    solid_fluid = NULL;
  }
  goto exit;
}

void
stardis_release_data(void* data)
{
  ASSERT(data);
  mem_rm(data);
}

/******************************************************************************
 * Solid fluid connection
 ******************************************************************************/
double
stardis_emissivity
  (const struct stardis_interface_fragment* frag,
   const unsigned source_id,
   void* data)
{
  const struct solid_fluid* solid_fluid = data;
  ASSERT(solid_fluid);
  (void)source_id, (void)frag;
  return solid_fluid->emissivity;
}

double
stardis_specular_fraction
  (const struct stardis_interface_fragment* frag,
   const unsigned source_id,
   void* data)
{
  const struct solid_fluid* solid_fluid = data;
  ASSERT(solid_fluid);
  (void)source_id, (void)frag;
  return solid_fluid->specular_fraction;
}

double
stardis_convection_coefficient
  (const struct stardis_interface_fragment* frag,
   void* data)
{
  const struct solid_fluid* solid_fluid = data;
  ASSERT(solid_fluid);
  (void)frag;
  return solid_fluid->convection_coef;
}

double
stardis_max_convection_coefficient(void* data)
{
  const struct solid_fluid* solid_fluid = data;
  ASSERT(solid_fluid);
  return solid_fluid->convection_coef;
}

double
stardis_reference_temperature
  (const struct stardis_interface_fragment* frag,
   void* data)
{
  const struct solid_fluid* solid_fluid = data;
  ASSERT(solid_fluid);
  (void)frag;
  return solid_fluid->tref;;
}

double*
stardis_t_range(void* data, double range[2])
{
  const struct solid_fluid* solid_fluid = data;
  ASSERT(solid_fluid);
  range[0] = range[1] = solid_fluid->tref;
  return range;
}

/*******************************************************************************
 * Legal notices
 ******************************************************************************/
const char*
get_copyright_notice(void* data)
{
  (void)data; /* Avoid "unused variable" warnings */
  return "Copyright (C) 2024 |Méso|Star> (contact@meso-star.com)";
}

const char*
get_license_short(void* data)
{
  (void)data; /* Avoid "unused variable" warnings */
  return "GNU GPL version 3 or later <http://www.gnu.org/licenses/>";
}

const char*
get_license_text(void* data)
{
  (void)data; /* Avoid "unused variable" warnings */
  return
    "This is free software released under the GPL v3+ license: GNU GPL\n"
    "version 3 or later. You are welcome to redistribute it under certain\n"
    "conditions; refer to <http://www.gnu.org/licenses/> for details.";
}
