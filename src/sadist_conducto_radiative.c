/* Copyright (C) 2024 |Méso|Star> (contact@meso-star.com)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>. */

#define _POSIX_C_SOURCE 200112L /* popen */

#include "sadist.h"

#include <rsys/rsys.h>
#include <rsys/math.h>

#include <errno.h>
#include <stdio.h>
#include <string.h> /* strerror */
#include <wait.h> /* WIFEXITED, WEXITSTATUS */

/*
 * The scene is a solid cube whose +/-X faces exchange with the radiative
 * environment. All other faces are adiabatic. The radiative environment is set
 * at T0 along the -X direction and T1 along the +X direction.
 *
 * This test calculates the steady temperature at a given position of the probe
 * in the solid and validates it against its analytical solution.
 *
 *                           //////(1,1,1)
 *                          +-------+
 *              --->       /'      /|      <---
 *         T0 K --->      +-------+ | E=1  <--- T1 K
 *    Y         --->  E=1 | +.....|.+      <---
 *    |                   |,      |/
 *    o--X                +-------+
 *   /                 (0,0,0) ////
 * Z
 */

#define FILENAME_CUBE "cube.stl"
#define FILENAME_ADIABATIC "adiabatic_boundary.stl"
#define FILENAME_RADIATIVE "radiative_boundary.stl"
#define FILENAME_SCENE "scene.txt"

#define EMISSIVITY 1.0
#define TREF 315.0 /* [K] */
#define T0 280.0 /* [K] */
#define T1 350.0 /* [K] */
#define LAMBDA 0.1

#define BOLTZMANN_CONSTANT 5.6696e-8 /* [W/m^2/K^4] */
#define HR (4.0*BOLTZMANN_CONSTANT * TREF*TREF*TREF * EMISSIVITY)

/* Probe position */
#define PX 0.2
#define PY 0.4
#define PZ 0.6

/* The commands to be executed */
#define COMMAND1 "stardis -V3 -p "STR(PX)","STR(PY)","STR(PZ)" -M "FILENAME_SCENE
#define COMMAND2 COMMAND1 " -a wos"

static const double vertices[] = {
  0.0, 0.0, 0.0,
  1.0, 0.0, 0.0,
  0.0, 1.0, 0.0,
  1.0, 1.0, 0.0,
  0.0, 0.0, 1.0,
  1.0, 0.0, 1.0,
  0.0, 1.0, 1.0,
  1.0, 1.0, 1.0
};
static const size_t nvertices = sizeof(vertices) / (sizeof(double)*3);

static const size_t indices[] = {
  0, 4, 2, 2, 4, 6, /* -x */
  3, 7, 5, 5, 1, 3, /* +x */
  0, 1, 5, 5, 4, 0, /* -y */
  2, 6, 7, 7, 3, 2, /* +y */
  0, 2, 1, 1, 2, 3, /* -z */
  4, 5, 6, 6, 5, 7  /* +z */
};
static const size_t ntriangles = sizeof(indices) / (sizeof(size_t)*3);

/*******************************************************************************
 * Helper functions
 ******************************************************************************/
static void
setup_scene(FILE* fp)
{
  ASSERT(fp);
  fprintf(fp, "PROGRAM radenv_1d libsadist_radenv_1d.so\n");
  fprintf(fp, "PROGRAM solid_fluid libsadist_solid_fluid.so\n");
  fprintf(fp, "FLUID environment 1 1 0 UNKNOWN BACK %s\n", FILENAME_CUBE);
  fprintf(fp, "SOLID cube %g 1 1 0.05 0 UNKNOWN 0 FRONT %s\n",
    LAMBDA, FILENAME_CUBE);
  fprintf(fp, "SOLID_FLUID_CONNECTION_PROG radiative solid_fluid %s "
    "PROG_PARAMS -e %g -r 300\n", FILENAME_RADIATIVE, EMISSIVITY);
  fprintf(fp, "SOLID_FLUID_CONNECTION adiabatic 0 0 0 0 %s\n",
    FILENAME_ADIABATIC);
  fprintf(fp, "TRAD_PROG radenv_1d PROG_PARAMS -r %g,%g -t %g,%g\n",
    TREF, TREF, T0, T1);
}

static int
init(void)
{
  FILE* fp_cube = NULL;
  FILE* fp_adiabatic = NULL;
  FILE* fp_radiative = NULL;
  FILE* fp_scene = NULL;
  int err = 0;

  #define FOPEN(Fp, Filename) \
    if(((Fp) = fopen((Filename), "w")) == NULL) { \
      fprintf(stderr, "Error opening `%s' -- file %s\n", \
        (Filename), strerror(errno)); \
      err = errno; \
      goto error; \
    } (void)0
  FOPEN(fp_cube, FILENAME_CUBE);
  FOPEN(fp_adiabatic, FILENAME_ADIABATIC);
  FOPEN(fp_radiative, FILENAME_RADIATIVE);
  FOPEN(fp_scene, FILENAME_SCENE);
  #undef FOPEN

  sadist_write_stl(fp_cube, vertices, nvertices, indices, ntriangles);
  sadist_write_stl(fp_adiabatic, vertices, nvertices, indices+12, ntriangles-4);
  sadist_write_stl(fp_radiative, vertices, nvertices, indices, 4);
  setup_scene(fp_scene);

exit:
  #define FCLOSE(Fp) \
    if((Fp) && fclose(Fp)) { perror("fclose"); if(!err) err = errno; } (void)0
  FCLOSE(fp_cube);
  FCLOSE(fp_adiabatic);
  FCLOSE(fp_radiative);
  FCLOSE(fp_scene);
  #undef FCLOSE
  return err;
error:
  goto exit;
}

static double
analytical_solution(void)
{
  const double tmp = LAMBDA / (2*LAMBDA + HR) * (T1 - T0);
  const double Ts0 = T0 + tmp;
  const double Ts1 = T1 - tmp;
  const double ref = PX*Ts1 + (1 - PX)*Ts0;
  return ref;
}

static int
run(const char* command)
{
  FILE* output = NULL;
  double E = 0;
  double SE = 0;
  double ref = 0;
  int n = 0;
  int err = 0;
  int status = 0;

  printf("%s\n", command);

  if(!(output = popen(command, "r"))) {
    fprintf(stderr, "Error executing stardis -- %s\n", strerror(errno));
    err = errno;
    goto error;
  }

  if((n = fscanf(output, "%lf %lf %*d %*d", &E, &SE)), n != 2 && n != EOF) {
    fprintf(stderr, "Error reading the output stream -- %s\n", strerror(errno));
    err = errno;
    goto error;
  }

  /* Check command exit status */
  if((status=pclose(output)), output=NULL, status) {
    if(status == -1) err = errno;
    else if(WIFEXITED(status)) err = WEXITSTATUS(status);
    else if(WIFSIGNALED(status)) err = WTERMSIG(status);
    else if(WIFSTOPPED(status)) err = WSTOPSIG(status);
    else FATAL("Unreachable code.\n");
    goto error;
  }

  ref = analytical_solution();
  printf("T = %g ~ %g +/- %g\n", ref, E, SE);
  if(!eq_eps(ref, E, SE*3)) {
    err = 1;
    goto error;
  }

exit:
  if(output) pclose(output);
  return err;
error:
  goto exit;
}

/*******************************************************************************
 * The test
 ******************************************************************************/
int
main(int argc, char** argv)
{
  int err = 0;
  (void)argc, (void)argv;

  if((err = init())) goto error;
  if((err = run(COMMAND1))) goto error;
  if((err = run(COMMAND2))) goto error;

exit:
  return err;
error:
  goto exit;
}
