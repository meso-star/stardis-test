/* Copyright (C) 2024 |Méso|Star> (contact@meso-star.com)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>. */

#define _POSIX_C_SOURCE 200112L /* popen */

#include "sadist.h"

#include <rsys/math.h>

#include <math.h>
#include <string.h> /* strerror */
#include <wait.h> /* WIFEXITED, WEXITSTATUS */

/*
 * The system consists of 2 parallelepipeds: a vertical one called the wall, and
 * a horizontal one representing the floor. The wall is a black body, while the
 * floor is a perfectly reflective surface. The surrounding fluid has a fixed
 * temperature and, finally, an external spherical source represents the sun.
 * This test calculates the steady temperature at a position in the wall and
 * compares it with the analytical solution given for a perfectly diffuse or
 * specular ground.
 *
 *  (-0.1,1500)
 *         +---+                                  External source
 *         |  E=1          T_FLUID                      ##
 *     Probe x |             _\                        ####
 *         |   |            / /                         ##
 *         +---+            \__/
 *            (0,500)
 *
 *            (0,0)
 *    Y        *--------E=0------------- - - -
 *    |        |
 *    o--X     *------------------------ - - -
 *   /        (0,-1)
 *  Z
 *
 */
#define FILENAME_GROUND "ground.stl"
#define FILENAME_WALL "wall.stl"
#define FILENAME_SCENE "scene.txt"
#define FILENAME_SCENE_PROG "scene2.txt"

#define SOURCE_ELEVATION 30.0 /* [degree] */
#define SOURCE_DISTANCE 1.5e11 /* [m] */
#define SOURCE_RADIUS 6.5991756e8
#define SOURCE_POWER 3.845e26 /* [W] */

/* Probe position */
#define PX -0.05
#define PY 1000.0
#define PZ 0.0

/* The Commands and the expected temperatures */
#define COMMAND1 "stardis -i -V3 -p "STR(PX)","STR(PY)","STR(PZ)" -M "FILENAME_SCENE
#define COMMAND2 "stardis -i -V3 -p "STR(PX)","STR(PY)","STR(PZ)\
  " -M "FILENAME_SCENE_PROG" -n 100000"
#define T_REF1 375.88 /* [K] */
#define T_REF2 417.77 /* [K] */

static const double ground_vertices[] = {
  0.0,    -1.0, -1.0e6,
  1.0e12, -1.0, -1.0e6,
  0.0,     1.0, -1.0e6,
  1.0e12,  1.0, -1.0e6,
  0.0,    -1.0,  1.0e6,
  1.0e12, -1.0,  1.0e6,
  0.0,     1.0,  1.0e6,
  1.0e12,  1.0,  1.0e6
};
static const size_t ground_nvertices = sizeof(ground_vertices)/(sizeof(double)*3);

static const double wall_vertices[] = {
  -0.1,  500.0, -500.0,
   0.0,  500.0, -500.0,
  -0.1, 1500.0, -500.0,
   0.0, 1500.0, -500.0,
  -0.1,  500.0,  500.0,
   0.0,  500.0,  500.0,
  -0.1, 1500.0,  500.0,
   0.0, 1500.0,  500.0
};
static const size_t wall_nvertices = sizeof(wall_vertices)/(sizeof(double)*3);

/* Ground and the wall indices */
static const size_t indices[] = {
  0, 2, 1, 1, 2, 3, /* -z */
  0, 4, 2, 2, 4, 6, /* -x */
  4, 5, 6, 6, 5, 7, /* +z */
  3, 7, 5, 5, 1, 3, /* +x */
  2, 6, 7, 7, 3, 2, /* +y */
  0, 1, 5, 5, 4, 0, /* -y */
};

static const size_t ntriangles = sizeof(indices) / (sizeof(size_t)*3);

/*******************************************************************************
 * Helper functions
 ******************************************************************************/
static void
setup_scene(FILE* fp)
{
  const double elevation = MDEG2RAD(SOURCE_ELEVATION);
  double pos[3];
  ASSERT(fp);

  fprintf(fp, "SOLID ground 1.15 1700 800 5.0e-3 0 UNKNOWN 0 FRONT "
    FILENAME_GROUND "\n");
  fprintf(fp, "SOLID wall 1.15 1700 800 5.0e-3 0 UNKNOWN 0 FRONT "
    FILENAME_WALL "\n");
  fprintf(fp, "FLUID dummy 1 1 300 300 BACK "FILENAME_GROUND" BACK "FILENAME_WALL "\n");
  fprintf(fp, "SOLID_FLUID_CONNECTION fluid_ground 0 0 0 0 "FILENAME_GROUND "\n");
  fprintf(fp, "SOLID_FLUID_CONNECTION fluid_wall 0 1 0 10 "FILENAME_WALL "\n");
  fprintf(fp, "TRAD 0 0\n");

  pos[0] = cos(elevation) * SOURCE_DISTANCE;
  pos[1] = sin(elevation) * SOURCE_DISTANCE;
  pos[2] = 0;
  fprintf(fp, "SPHERICAL_SOURCE 6.5991756e8 %f %f %f 3.845e26 0\n",
    pos[0], pos[1], pos[2]);
}

static void
setup_scene_prog(FILE* fp)
{
  const double elevation = MDEG2RAD(SOURCE_ELEVATION);
  double pos[3];
  ASSERT(fp);

  fprintf(fp, "PROGRAM source libsadist_spherical_source.so\n");
  fprintf(fp, "SOLID ground 1.15 1700 800 5.0e-3 0 UNKNOWN 0 FRONT "
    FILENAME_GROUND "\n");
  fprintf(fp, "SOLID wall 1.15 1700 800 5.0e-3 0 UNKNOWN 0 FRONT "
    FILENAME_WALL "\n");
  fprintf(fp, "FLUID dummy 1 1 300 300 BACK "FILENAME_GROUND" BACK "FILENAME_WALL "\n");
  fprintf(fp, "SOLID_FLUID_CONNECTION fluid_ground 0 0 1 0 "FILENAME_GROUND "\n");
  fprintf(fp, "SOLID_FLUID_CONNECTION fluid_wall 0 1 0 10 "FILENAME_WALL "\n");
  fprintf(fp, "TRAD 0 0\n");

  pos[0] = cos(elevation) * SOURCE_DISTANCE;
  pos[1] = sin(elevation) * SOURCE_DISTANCE;
  pos[2] = 0;

  fprintf(fp, "SPHERICAL_SOURCE_PROG "STR(SOURCE_RADIUS)" source "
    "PROG_PARAMS -d 0 -p %f,%f,%f -w "STR(SOURCE_POWER)"\n",
    pos[0], pos[1], pos[2]);
}

static int
init(void)
{
  FILE* fp_ground = NULL;
  FILE* fp_wall = NULL;
  FILE* fp_scene = NULL;
  FILE* fp_scene_prog = NULL;
  int err = 0;

  if((fp_ground = fopen(FILENAME_GROUND, "w")) == NULL) {
    fprintf(stderr, "Error opening the `"FILENAME_GROUND"' file -- %s\n",
      strerror(errno));
    err = errno;
    goto error;
  }
  if((fp_wall = fopen(FILENAME_WALL, "w")) == NULL) {
    fprintf(stderr, "Error opening the `"FILENAME_WALL "' file -- %s\n",
      strerror(errno));
    err = errno;
    goto error;
  }
  if((fp_scene = fopen(FILENAME_SCENE, "w")) == NULL) {
    fprintf(stderr, "Error opening the `"FILENAME_SCENE"' file -- %s\n",
      strerror(errno));
    err = errno;
    goto error;
  }
  if((fp_scene_prog = fopen(FILENAME_SCENE_PROG, "w")) == NULL) {
    fprintf(stderr, "Error opening the `"FILENAME_SCENE_PROG"' file -- %s\n",
      strerror(errno));
    err = errno;
    goto error;
  }

  sadist_write_stl(fp_ground, ground_vertices, ground_nvertices, indices, ntriangles);
  sadist_write_stl(fp_wall, wall_vertices, wall_nvertices, indices, ntriangles);
  setup_scene(fp_scene);
  setup_scene_prog(fp_scene_prog);

exit:
  if(fp_ground && fclose(fp_ground)) { perror("fclose"); if(!err) err = errno; }
  if(fp_wall && fclose(fp_wall)) { perror("fclose"); if(!err) err = errno; }
  if(fp_scene && fclose(fp_scene)) { perror("fclose"); if(!err) err = errno; }
  if(fp_scene_prog && fclose(fp_scene_prog)) { perror("fclose"); if(!err) err = errno; }
  return err;
error:
  goto exit;
}

static int
run(const char* command, const double ref)
{
  FILE* output = NULL;
  double E = 0;
  double SE = 0;
  int n = 0;
  int err = 0;
  int status = 0;

  printf("%s\n", command);

  if(!(output = popen(command, "r"))) {
    fprintf(stderr, "Error executing stardis -- %s\n", strerror(errno));
    err = errno;
    goto error;
  }

  if((n = fscanf(output, "%lf %lf %*d %*d", &E, &SE)), n != 2 && n != EOF) {
    fprintf(stderr, "Error reading the output stream -- %s\n", strerror(errno));
    err = errno;
    goto error;
  }

  /* Check command exit status */
  if((status=pclose(output)), output=NULL, status) {
    if(status == -1) err = errno;
    else if(WIFEXITED(status)) err = WEXITSTATUS(status);
    else if(WIFSIGNALED(status)) err = WTERMSIG(status);
    else if(WIFSTOPPED(status)) err = WSTOPSIG(status);
    else FATAL("Unreachable code.\n");
    goto error;
  }

  printf("T = %g ~ %g +/- %g\n", ref, E, SE);
  if(!eq_eps(ref, E, SE*3)) {
    err = 1;
    goto error;
  }

exit:
  if(output) pclose(output);
  return err;
error:
  goto exit;
}

/*******************************************************************************
 * The test
 ******************************************************************************/
int
main(int argc, char** argv)
{
  int err = 0;
  (void)argc, (void)argv;

  if((err = init())) goto error;
  if((err = run(COMMAND1, T_REF1))) goto error;
  if((err = run(COMMAND2, T_REF2))) goto error;

exit:
  return err;
error:
  goto exit;
}
