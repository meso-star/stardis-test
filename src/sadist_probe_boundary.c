/* Copyright (C) 2024 |Méso|Star> (contact@meso-star.com)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>. */

#define _POSIX_C_SOURCE 200112L /* popen */

#include "sadist.h"

#include <stardis/stardis-prog-properties.h>

#include <star/s3dut.h>

#include <rsys/cstr.h>
#include <rsys/double2.h>
#include <rsys/double3.h>
#include <rsys/math.h>
#include <rsys/mem_allocator.h>

#include <errno.h>
#include <float.h>
#include <getopt.h>
#include <string.h> /* strerror */
#include <wait.h> /* WIFEXITED, WEXITSTATUS */

/* Axis Aligned Bounding Box */
struct aabb {
  double lower[3];
  double upper[3];
};
#define AABB_NULL__ {{DBL_MAX,DBL_MAX,DBL_MAX}, {-DBL_MAX,-DBL_MAX,-DBL_MAX}}
static const struct aabb AABB_NULL = AABB_NULL__;

struct args {
  unsigned nprobes; /* Number of probes to solve */
  int quit;
};
#define ARGS_DEFAULT__ {1, 0}
static const struct args ARGS_DEFAULT = ARGS_DEFAULT__;

/*
 * The system is a trilinear profile of the temperature at steady state, i.e. at
 * each point of the system we can calculate the temperature analytically. Two
 * forms are immersed in this temperature field: a super shape and a sphere
 * included in the super shape. On the Monte Carlo side, the temperature is
 * unknown everywhere  except on the surface of the super shape whose
 * temperature is defined from the aformentionned trilinear profile. We will
 * estimate the temperature at the sphere boundary at a probe points. We
 * should find by Monte Carlo the temperature of the trilinear profile at the
 * position of the probe. It's the test.
 *
 *                       /\ <-- T(x,y,z)
 *                   ___/  \___
 *      T(z)         \   __   /
 *       |  T(y)      \ /  \ /
 *       |/         T=? *__/ \
 *       o--- T(x)   /_  __  _\
 *                     \/  \/
 */

#define FILENAME_SSHAPE "sshape.stl"
#define FILENAME_SPHERE "sphere.stl"
#define FILENAME_PROBES "probes.txt"
#define FILENAME_SCENE "scene.txt"

/* Temperature at the upper bound of the X, Y and Z axis. The temperature at the
 * lower bound is implicitly 0 K */
#define TX 333.0 /* [K] */
#define TY 432.0 /* [K] */
#define TZ 579.0 /* [K] */

/* Probe position */
#define PX 1.0
#define PY 0.0
#define PZ 0.0

/* Commands to calculate 1 or N probes */
#define COMMAND1 "stardis -V3 -P "STR(PX)","STR(PY)","STR(PZ)" -M "FILENAME_SCENE
#define COMMANDN "stardis -V3 -L "FILENAME_PROBES" -M "FILENAME_SCENE
#define COMMANDN_MPI "mpirun -n 2 "COMMANDN

/*******************************************************************************
 * Helper functions
 ******************************************************************************/
static double
rand_canonic(void)
{
  return (double)rand() / (double)((long)RAND_MAX - 1);
}

static int
is_mpi_enabled(void)
{
  char buf[128] = {0};
  FILE* sdis = NULL;
  FILE* grep = NULL;

  if(!(sdis = popen("stardis -v", "r")))
    perror("stardis"), exit(errno);

  if(!(grep = popen("grep -qe \" MPI is enabled\"", "w")))
    perror("grep"), exit(errno);

  while(fgets(buf, sizeof(buf), sdis) && !feof(sdis)) {
    if(fputs(buf, grep) == EOF) abort();
  }

  if(ferror(sdis)) errno=EIO, perror("stardis"), exit(errno);
  if(pclose(sdis) == -1) perror("stardis"), exit(errno);
  return !pclose(grep);
}

static void
sample_unit_sphere(double p[3])
{
  const double phi = rand_canonic() * 2*PI;
  const double v = rand_canonic();
  const double cos_theta = 1 - 2*v;
  const double sin_theta = 2 * sqrt(v*(1-v));
  p[0] = cos(phi) * sin_theta;
  p[1] = sin(phi) * sin_theta;
  p[2] = cos_theta;
}

static void
aabb_update(struct aabb* aabb, const struct s3dut_mesh_data* mesh)
{
  size_t ivertex = 0;
  ASSERT(aabb);

  FOR_EACH(ivertex, 0, mesh->nvertices) {
    const double* vertex = mesh->positions + ivertex*3;
    aabb->lower[0] = MMIN(aabb->lower[0], vertex[0]);
    aabb->lower[1] = MMIN(aabb->lower[1], vertex[1]);
    aabb->lower[2] = MMIN(aabb->lower[2], vertex[2]);
    aabb->upper[0] = MMAX(aabb->upper[0], vertex[0]);
    aabb->upper[1] = MMAX(aabb->upper[1], vertex[1]);
    aabb->upper[2] = MMAX(aabb->upper[2], vertex[2]);
  }
}

static void
setup_sshape(FILE* stream, struct aabb* aabb)
{
  struct s3dut_mesh* sshape = NULL;
  struct s3dut_mesh_data sshape_data;
  struct s3dut_super_formula f0 = S3DUT_SUPER_FORMULA_NULL;
  struct s3dut_super_formula f1 = S3DUT_SUPER_FORMULA_NULL;
  const double radius = 2;
  const unsigned nslices = 256;

  f0.A = 1.5; f0.B = 1; f0.M = 11.0; f0.N0 = 1; f0.N1 = 1; f0.N2 = 2.0;
  f1.A = 1.0; f1.B = 2; f1.M =  3.6; f1.N0 = 1; f1.N1 = 2; f1.N2 = 0.7;
  S3DUT(create_super_shape(NULL, &f0, &f1, radius, nslices, nslices/2, &sshape));
  S3DUT(mesh_get_data(sshape, &sshape_data));

  aabb_update(aabb, &sshape_data);
  sadist_write_stl(stream, sshape_data.positions, sshape_data.nvertices,
    sshape_data.indices, sshape_data.nprimitives);

  S3DUT(mesh_ref_put(sshape));
}

static void
setup_sphere(FILE* stream, struct aabb* aabb)
{
  struct s3dut_mesh* sphere = NULL;
  struct s3dut_mesh_data sphere_data;
  const double radius = 1;
  const unsigned nslices = 128;

  S3DUT(create_sphere(NULL, radius, nslices, nslices/2, &sphere));
  S3DUT(mesh_get_data(sphere, &sphere_data));

  aabb_update(aabb, &sphere_data);
  sadist_write_stl(stream, sphere_data.positions, sphere_data.nvertices,
    sphere_data.indices, sphere_data.nprimitives);

  S3DUT(mesh_ref_put(sphere));
}

static void
setup_scene
  (FILE* fp,
   const char* sshape,
   const char* sphere,
   const struct aabb* aabb,
   struct sadist_trilinear_profile* profile)
{
  double low, upp;
  ASSERT(sshape && sphere && aabb && profile);

  fprintf(fp, "PROGRAM trilinear_profile libsadist_trilinear_profile.so\n");
  fprintf(fp, "SOLID SuperShape 25 7500 500 0.05 0 UNKNOWN 0 BACK %s FRONT %s\n",
    sshape, sphere);
  fprintf(fp, "SOLID Sphere 25 7500 500 0.05 0 UNKNOWN 0 BACK  %s\n", sphere);

  low = MMIN(MMIN(aabb->lower[0], aabb->lower[1]), aabb->lower[2]);
  upp = MMAX(MMAX(aabb->upper[0], aabb->upper[1]), aabb->upper[2]);
  fprintf(fp, "T_BOUNDARY_FOR_SOLID_PROG Dirichlet trilinear_profile %s", sshape);
  fprintf(fp, " PROG_PARAMS -b %g,%g -t %g,%g,%g\n", low, upp, TX, TY, TZ);

  d3_splat(profile->lower, low);
  d3_splat(profile->upper, upp);
  d2(profile->a, 0, TX);
  d2(profile->b, 0, TY);
  d2(profile->c, 0, TZ);
}

static void
usage(const char* name, FILE* stream)
{
  ASSERT(name && stream);
  fprintf(stream, "usage: %s [-h] [-p probes_count]\n", name);
}

static int
args_init(struct args* args, int argc, char** argv)
{
  int opt = 0;
  int err = 0;

  ASSERT(args);
  while((opt = getopt(argc, argv, "hp:")) != -1) {
    switch(opt) {
      case 'h':
        usage(argv[0], stdout);
        args->quit = 1;
        break;
      case 'p':
        err = (cstr_to_uint(optarg, &args->nprobes) != RES_OK);
        if(!err && args->nprobes == 0) err = 1;
        break;
      default: err = 1; break;
    }
    if(err) {
      if(optarg) {
        fprintf(stderr, "%s: invalid option argument `%s' -- `%c'\n",
          argv[0], optarg, opt);
      }
      goto error;
    }
  }

exit:
  return err;
error:
  usage(argv[0], stderr);
  goto exit;
}

static int
init(struct sadist_trilinear_profile* profile)
{
  struct aabb aabb = AABB_NULL;
  FILE* fp_sshape = NULL;
  FILE* fp_sphere = NULL;
  FILE* fp_scene = NULL;
  int err = 0;

  if((fp_sshape = fopen(FILENAME_SSHAPE, "w")) == NULL) {
    fprintf(stderr, "Error opening the `"FILENAME_SSHAPE"' file -- %s\n",
      strerror(errno));
    err = errno;
    goto error;
  }
  if((fp_sphere = fopen(FILENAME_SPHERE, "w")) == NULL) {
    fprintf(stderr, "Error opening the `"FILENAME_SPHERE"' file -- %s\n",
      strerror(errno));
    err = errno;
    goto error;
  }
  if((fp_scene = fopen(FILENAME_SCENE, "w")) == NULL) {
    fprintf(stderr, "Error opening the `"FILENAME_SCENE"' file -- %s\n",
      strerror(errno));
    err = errno;
    goto error;
  }

  setup_sshape(fp_sshape, &aabb);
  setup_sphere(fp_sphere, &aabb);
  setup_scene(fp_scene, FILENAME_SSHAPE, FILENAME_SPHERE, &aabb, profile);

exit:
  if(fp_sshape && fclose(fp_sshape)) { perror("fclose"); if(!err) err = errno; }
  if(fp_sphere && fclose(fp_sphere)) { perror("fclose"); if(!err) err = errno; }
  if(fp_scene && fclose(fp_scene)) { perror("fclose"); if(!err) err = errno; }
  return err;
error:
  goto exit;
}

static int
init_probe_list(double* probes, const size_t nprobes)
{
  FILE* fp_probes = NULL;
  size_t i = 0;
  int err = 0;
  ASSERT(probes && nprobes);

  if((fp_probes = fopen(FILENAME_PROBES, "w")) == NULL) {
    fprintf(stderr, "Error opening `"FILENAME_PROBES"' file -- %s\n",
      strerror(errno));
    err = errno;
    goto error;
  }

  FOR_EACH(i, 0, nprobes) {
    sample_unit_sphere(probes+i*3);
    if(fprintf(fp_probes, "%g,%g,%g\n", SPLIT3(probes+i*3)) < 0) {
      fprintf(stderr, "Error writing probes -- %s\n", strerror(errno));
      err = errno;
      goto error;
    }
  }

exit:
  if(fp_probes && fclose(fp_probes)) { perror("fclose"); if(!err) err = errno; }
  return err;
error:
  goto exit;
}

static int
run1(const struct sadist_trilinear_profile* profile)
{
  const double P[3] = {PX, PY, PZ};
  FILE* output = NULL;
  double ref = 0;
  double E = 0;
  double SE = 0;
  int n = 0;
  int err = 0;
  int status = 0;

  printf(COMMAND1"\n");

  if(!(output = popen(COMMAND1, "r"))) {
    fprintf(stderr, "Error executing stardis -- %s\n", strerror(errno));
    fprintf(stderr, "\t"COMMAND1"\n");
    err = errno;
    goto error;
  }

  if((n = fscanf(output, "%lf %lf %*d %*d", &E, &SE)), n != 2 && n != EOF) {
    fprintf(stderr, "Error reading the output stream -- %s\n", strerror(errno));
    err = errno;
    goto error;
  }

  /* Check command exit status */
  if((status=pclose(output), output=NULL, status)) {
    if(status == -1) err = errno;
    else if(WIFEXITED(status)) err = WEXITSTATUS(status);
    else if(WIFSIGNALED(status)) err = WTERMSIG(status);
    else if(WIFSTOPPED(status)) err = WSTOPSIG(status);
    else FATAL("Unreachable code.\n");
    goto error;
  }

  ref = sadist_trilinear_profile_temperature(profile, P);
  printf("T = %g ~ %g +/- %g\n", ref, E, SE);
  if(!eq_eps(ref, E, SE*3)) {
    err = 1;
    goto error;
  }

exit:
  if(output) pclose(output);
  return err;
error:
  goto exit;
}

static int
runN(const struct sadist_trilinear_profile* profile, const size_t nprobes)
{
  const char* command = NULL;
  double* probes = NULL; /* Positions */
  double* results = NULL; /* Expected values and standard deviations */
  FILE* output = NULL;
  size_t i = 0;
  int err = 0;
  int status = 0;
  ASSERT(profile && nprobes);

  if(!(probes = mem_calloc(nprobes, sizeof(double)*3))) {
    err = errno = ENOMEM;
    perror("mem_calloc");
    goto error;
  }

  if(!(results = mem_calloc(nprobes, sizeof(double)*2))) {
    err = errno = ENOMEM;
    perror("mem_calloc");
    goto error;
  }

  if((err = init_probe_list(probes, nprobes))) goto error;

  if(is_mpi_enabled()) {
    command = COMMANDN_MPI;
  } else {
    command = COMMANDN;
  }
  printf("%s\n", command);

  if(!(output = popen(command, "r"))) {
    fprintf(stderr, "Error executing stardis -- %s\n", strerror(errno));
    fprintf(stderr, "\t%s\n", command);
    err = errno;
    goto error;
  }

  /* Fetch the result */
  FOR_EACH(i, 0, nprobes) {
    double* E = results + i*2 + 0;
    double* SE = results + i*2 + 1;
    int n = 0;

    if((n = fscanf(output, "%lf %lf %*d %*d", E, SE)), n != 2 && n != EOF) {
      perror("fscanf");
      err = errno;
      goto error;
    }
  }

  /* Check command exit status */
  if((status=pclose(output)), output = NULL, status) {
    if(status == -1) err = errno;
    else if(WIFEXITED(status)) err = WEXITSTATUS(status);
    else if(WIFSIGNALED(status)) err = WTERMSIG(status);
    else if(WIFSTOPPED(status)) err = WSTOPSIG(status);
    else FATAL("Unreachable code.\n");
    goto error;
  }
  output = NULL;

  /* Validate the calculations */
  FOR_EACH(i, 0, nprobes) {
    const double* P = probes + i*3;
    const double E = results[i*2 + 0];
    const double SE = results[i*2 + 1];
    const double ref = sadist_trilinear_profile_temperature(profile, P);

    printf("T = %g ~ %g +/- %g\n", ref, E, SE);
    if(!eq_eps(ref, E, SE*3)) {
      err = 1;
      goto error;
    }
  }

exit:
  if(probes) mem_rm(probes);
  if(results) mem_rm(results);
  if(output) pclose(output);
  return err;
error:
  goto exit;
}

/*******************************************************************************
 * The test
 ******************************************************************************/
int
main(int argc, char** argv)
{
  struct args args = ARGS_DEFAULT;
  struct sadist_trilinear_profile profile = SADIST_TRILINEAR_PROFILE_NULL;
  int err = 0;

  if((err = args_init(&args, argc, argv))) goto error;
  if(args.quit) goto exit;

  if((err = init(&profile))) goto error;

  if(args.nprobes == 1) {
    if((err = run1(&profile))) goto error;
  } else {
    if((err = runN(&profile, args.nprobes))) goto error;
  }

exit:
  if(mem_allocated_size() != 0) {
    fprintf(stderr, "Memory leaks: %lu bytes\n", mem_allocated_size());
    if(!err) err = -1;
  }
  return err;
error:
  goto exit;
}
