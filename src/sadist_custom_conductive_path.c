/* Copyright (C) 2024 |Méso|Star> (contact@meso-star.com)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>. */

#define _POSIX_C_SOURCE 200112L /* popen */

#include "sadist.h"

#include <stardis/stardis-prog-properties.h>

#include <star/s3dut.h>

#include <rsys/double2.h>
#include <rsys/double3.h>
#include <rsys/math.h>

#include <errno.h>
#include <float.h>
#include <getopt.h>
#include <string.h> /* strerror */
#include <wait.h> /* WIFEXITED, WEXITSTATUS */

/*
 * The system is a trilinear profile of the temperature at steady state, i.e. at
 * each point of the system we can calculate the temperature analytically. Two
 * forms are immersed in this temperature field: a super shape and a sphere
 * included in the super shape. On the Monte Carlo side, the temperature is
 * unknown everywhere  except on the surface of the super shape whose
 * temperature is defined from the aformentionned trilinear profile.
 *
 * We will estimate the temperature at the position of a probe in solids by
 * providing a user-side function to sample the conductive path in the sphere.
 * We should find the temperature of the trilinear profile at the probe position
 * by Monte Carlo, independently of this coupling with an external path sampling
 * routine.
 *
 *
 *                       /\ <-- T(x,y,z)
 *                   ___/  \___
 *      T(z)         \   __   /
 *       |  T(y)     T=?/. \ /
 *       |/           / \__/ \
 *       o--- T(x)   /_  __  _\
 *                     \/  \/
 */

#define FILENAME_SSHAPE "sshape.stl"
#define FILENAME_SPHERE "sphere.stl"
#define FILENAME_SCENE "scene.txt"

/* Temperature at the upper bound of the X, Y and Z axis. The temperature at the
 * lower bound is implicitly 0 K */
#define TX 333.0 /* [K] */
#define TY 432.0 /* [K] */
#define TZ 579.0 /* [K] */

/* Probe position */
#define PX 0.125
#define PY 0.250
#define PZ 0.375

/* Commands to calculate probe temperature */
#define COMMAND "stardis -V3 -p "STR(PX)","STR(PY)","STR(PZ)" -M "FILENAME_SCENE

/* Axis Aligned Bounding Box */
struct aabb {
  double lower[3];
  double upper[3];
};
#define AABB_NULL__ {{DBL_MAX,DBL_MAX,DBL_MAX}, {-DBL_MAX,-DBL_MAX,-DBL_MAX}}
static const struct aabb AABB_NULL = AABB_NULL__;


/*******************************************************************************
 * Helper functions
 ******************************************************************************/
static void
aabb_update(struct aabb* aabb, const struct s3dut_mesh_data* mesh)
{
  size_t ivertex = 0;
  ASSERT(aabb);

  FOR_EACH(ivertex, 0, mesh->nvertices) {
    const double* vertex = mesh->positions + ivertex*3;
    aabb->lower[0] = MMIN(aabb->lower[0], vertex[0]);
    aabb->lower[1] = MMIN(aabb->lower[1], vertex[1]);
    aabb->lower[2] = MMIN(aabb->lower[2], vertex[2]);
    aabb->upper[0] = MMAX(aabb->upper[0], vertex[0]);
    aabb->upper[1] = MMAX(aabb->upper[1], vertex[1]);
    aabb->upper[2] = MMAX(aabb->upper[2], vertex[2]);
  }
}

static void
setup_sshape(FILE* stream, struct aabb* aabb)
{
  struct s3dut_mesh* sshape = NULL;
  struct s3dut_mesh_data sshape_data;
  struct s3dut_super_formula f0 = S3DUT_SUPER_FORMULA_NULL;
  struct s3dut_super_formula f1 = S3DUT_SUPER_FORMULA_NULL;
  const double radius = 2;
  const unsigned nslices = 256;

  f0.A = 1.5; f0.B = 1; f0.M = 11.0; f0.N0 = 1; f0.N1 = 1; f0.N2 = 2.0;
  f1.A = 1.0; f1.B = 2; f1.M =  3.6; f1.N0 = 1; f1.N1 = 2; f1.N2 = 0.7;
  S3DUT(create_super_shape(NULL, &f0, &f1, radius, nslices, nslices/2, &sshape));
  S3DUT(mesh_get_data(sshape, &sshape_data));

  aabb_update(aabb, &sshape_data);
  sadist_write_stl(stream, sshape_data.positions, sshape_data.nvertices,
    sshape_data.indices, sshape_data.nprimitives);

  S3DUT(mesh_ref_put(sshape));
}

static void
setup_sphere(FILE* stream, struct aabb* aabb)
{
  struct s3dut_mesh* sphere = NULL;
  struct s3dut_mesh_data sphere_data;
  const double radius = 1;
  const unsigned nslices = 128;

  S3DUT(create_sphere(NULL, radius, nslices, nslices/2, &sphere));
  S3DUT(mesh_get_data(sphere, &sphere_data));

  aabb_update(aabb, &sphere_data);
  sadist_write_stl(stream, sphere_data.positions, sphere_data.nvertices,
    sphere_data.indices, sphere_data.nprimitives);

  S3DUT(mesh_ref_put(sphere));
}

static void
setup_scene
  (FILE* fp,
   const char* sshape,
   const char* sphere,
   const struct aabb* aabb,
   struct sadist_trilinear_profile* profile)
{
  double low, upp;
  ASSERT(sshape && sphere && aabb && profile);

  fprintf(fp, "PROGRAM solid_sphere libsadist_solid_sphere.so\n");
  fprintf(fp, "PROGRAM trilinear_profile libsadist_trilinear_profile.so\n");
  fprintf(fp, "SOLID SuperShape 25 7500 500 0.05 0 UNKNOWN 0 BACK %s FRONT %s\n",
    sshape, sphere);
  fprintf(fp, "SOLID_PROG Sphere solid_sphere BACK %s ", sphere);

  /* StL lambda rho cp radius */
  fprintf(fp, "PROG_PARAMS -m %s -l25 -r7500 -c500 -R1\n", FILENAME_SPHERE);

  low = MMIN(MMIN(aabb->lower[0], aabb->lower[1]), aabb->lower[2]);
  upp = MMAX(MMAX(aabb->upper[0], aabb->upper[1]), aabb->upper[2]);
  fprintf(fp, "T_BOUNDARY_FOR_SOLID_PROG Dirichlet trilinear_profile %s", sshape);
  fprintf(fp, " PROG_PARAMS -b %g,%g -t %g,%g,%g\n", low, upp, TX, TY, TZ);

  d3_splat(profile->lower, low);
  d3_splat(profile->upper, upp);
  d2(profile->a, 0, TX);
  d2(profile->b, 0, TY);
  d2(profile->c, 0, TZ);
}

static int
init(struct sadist_trilinear_profile* profile)
{
  struct aabb aabb = AABB_NULL;
  FILE* fp_sshape = NULL;
  FILE* fp_sphere = NULL;
  FILE* fp_scene = NULL;
  int err = 0;

  #define FOPEN(Fp, Filename) \
    if(((Fp) = fopen((Filename), "w")) == NULL) { \
      fprintf(stderr, "Error opening `%s' -- file %s\n", \
        (Filename), strerror(errno)); \
      err = errno; \
      goto error; \
    } (void)0
  FOPEN(fp_sshape, FILENAME_SSHAPE);
  FOPEN(fp_sphere, FILENAME_SPHERE);
  FOPEN(fp_scene, FILENAME_SCENE);
  #undef FOPEN

  setup_sshape(fp_sshape, &aabb);
  setup_sphere(fp_sphere, &aabb);
  setup_scene(fp_scene, FILENAME_SSHAPE, FILENAME_SPHERE, &aabb, profile);

exit:
  #define FCLOSE(Fp) \
    if((Fp) && fclose(Fp)) { perror("fclose"); if(!err) err = errno; } (void)0
  FCLOSE(fp_sshape);
  FCLOSE(fp_sphere);
  FCLOSE(fp_scene);
  #undef FCLOSE
  return err;
error:
  goto exit;
}

static int
run(const struct sadist_trilinear_profile* profile)
{
  const double P[3] = {PX, PY, PZ};

  FILE* output = NULL;
  double E = 0;
  double SE = 0;
  double ref = 0;

  int n = 0;
  int err = 0;
  int status = 0;

  printf("%s\n", COMMAND);

  if(!(output = popen(COMMAND, "r"))) {
    fprintf(stderr, "Error executing stardis -- %s\n", strerror(errno));
    err = errno;
    goto error;
  }

  if((n = fscanf(output, "%lf %lf %*d %*d", &E, &SE)), n != 2 && n != EOF) {
    fprintf(stderr, "Error reading the output stream -- %s\n", strerror(errno));
    err = errno;
    goto error;
  }

  /* Check command exit status */
  if((status=pclose(output)), output=NULL, status) {
    if(status == -1) err = errno;
    else if(WIFEXITED(status)) err = WEXITSTATUS(status);
    else if(WIFSIGNALED(status)) err = WTERMSIG(status);
    else if(WIFSTOPPED(status)) err = WSTOPSIG(status);
    else FATAL("Unreachable code.\n");
    goto error;
  }

  ref = sadist_trilinear_profile_temperature(profile, P);
  printf("T = %g ~ %g +/- %g\n", ref, E, SE);
  if(!eq_eps(ref, E, SE*3)) {
    err = 1;
    goto error;
  }

exit:
  if(output) pclose(output);
  return err;
error:
  goto exit;
}

/*******************************************************************************
 * The test
 ******************************************************************************/
int
main(int argc, char** argv)
{
  struct sadist_trilinear_profile profile = SADIST_TRILINEAR_PROFILE_NULL;
  int err;
  (void)argc, (void)argv;

  if((err = init(&profile))) goto error;
  if((err = run(&profile))) goto error;

exit:
  return err;
error:
  goto exit;
}
