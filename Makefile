# Copyright (C) 2024 |Méso|Star> (contact@meso-star.com)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.

.POSIX:
.SUFFIXES: # Clean up default inference rules

include config.mk

TESTS =\
 sadist_conducto_radiative\
 sadist_custom_conductive_path\
 sadist_external_flux\
 sadist_probe_boundary\
 sadist_unsteady

LIBS =\
 libsadist_radenv_1d.so\
 libsadist_solid_fluid.so\
 libsadist_solid_sphere.so\
 libsadist_spherical_source.so\
 libsadist_trilinear_profile.so\
 libsadist_unsteady_profile.so\

# Default target
default: .config $(TESTS) $(LIBS)

.config:
	@if ! $(PKG_CONFIG) --atleast-version $(RSYS_VERSION) rsys; then \
	  echo "rsys $(RSYS_VERSION) not found" >&2; exit 1; fi
	@if ! $(PKG_CONFIG) --atleast-version $(S3D_VERSION) s3d; then \
	  echo "s3d $(S3D_VERSION) not found" >&2; exit 1; fi
	@if ! $(PKG_CONFIG) --atleast-version $(S3DUT_VERSION) s3dut; then \
	  echo "s3dut $(S3DUT_VERSION) not found" >&2; exit 1; fi
	@if ! $(PKG_CONFIG) --atleast-version $(SSP_VERSION) star-sp; then \
	  echo "star-sp $(SSP_VERSION) not found" >&2; exit 1; fi
	@if ! $(PKG_CONFIG) --atleast-version $(SSTL_VERSION) sstl; then \
	  echo "sstl $(SSTL_VERSION) not found" >&2; exit 1; fi
	@echo "config done" > $@

clean:
	rm -f .config src/*.o src/*.d
	rm -f cube.stl ground.stl sphere.stl sshape.stl wall.stl
	rm -f adiabatic_boundary.stl radiative_boundary.stl
	rm -f probes.txt scene.txt scene2.txt
	rm -f $(TESTS) $(LIBS)

test: $(TESTS) $(LIBS)
	@$(SHELL) make.sh check sadist_conducto_radiative ./sadist_conducto_radiative
	@$(SHELL) make.sh check sadist_custom_conductive_path ./sadist_custom_conductive_path
	@$(SHELL) make.sh check sadist_probe_boundary ./sadist_probe_boundary
	@$(SHELL) make.sh check sadist_probe_boundary_list ./sadist_probe_boundary -p4
	@$(SHELL) make.sh check sadist_external_flux ./sadist_external_flux
	@$(SHELL) make.sh check sadist_unsteady ./sadist_unsteady

################################################################################
# Libraries
################################################################################
src/sadist_lib_radenv_1d.o: config.mk src/sadist_lib_radenv_1d.c
	$(CC) $(CFLAGS_SO) $(RSYS_CFLAGS) -c $(@:.o=.c) -o $@

libsadist_radenv_1d.so: config.mk src/sadist_lib_radenv_1d.o
	$(CC) $(CFLAGS_SO) $(RSYS_CFLAGS) -o $@ src/sadist_lib_radenv_1d.o \
	$(LDFLAGS_SO) $(RSYS_LIBS)

src/sadist_lib_solid_fluid.o: config.mk src/sadist_lib_solid_fluid.c
	$(CC) $(CFLAGS_SO) $(RSYS_CFLAGS) -c $(@:.o=.c) -o $@

libsadist_solid_fluid.so: config.mk src/sadist_lib_solid_fluid.o
	$(CC) $(CFLAGS_SO) $(RSYS_CFLAGS) -o $@ src/sadist_lib_solid_fluid.o \
	$(LDFLAGS_SO) $(RSYS_LIBS)

src/sadist_lib_solid_sphere.o: config.mk src/sadist_lib_solid_sphere.c
	$(CC) $(CFLAGS_SO) $(S3D_CFLAGS) $(SSP_CFLAGS) $(SSTL_CFLAGS) $(RSYS_CFLAGS) \
	-c $(@:.o=.c) -o $@

libsadist_solid_sphere.so: config.mk src/sadist_lib_solid_sphere.o
	$(CC) $(CFLAGS_SO) $(S3D_CFLAGS) $(SSP_CFLAGS) $(SSTL_CFLAGS) $(RSYS_CFLAGS) \
	-o $@ src/sadist_lib_solid_sphere.o $(LDFLAGS_SO) $(S3D_LIBS) $(SSP_LIBS) \
	$(SSTL_LIBS) $(RSYS_LIBS) -lm

src/sadist_lib_spherical_source.o: config.mk src/sadist_lib_spherical_source.c
	$(CC) $(CFLAGS_SO) $(RSYS_CFLAGS) -c $(@:.o=.c) -o $@

libsadist_spherical_source.so: config.mk src/sadist_lib_spherical_source.o
	$(CC) $(CFLAGS_SO) $(RSYS_CFLAGS) -o $@ src/sadist_lib_spherical_source.o \
	$(LDFLAGS_SO) $(RSYS_LIBS)

src/sadist_lib_trilinear_profile.o:\
 config.mk src/sadist.h src/sadist_lib_trilinear_profile.c
	$(CC) $(CFLAGS_SO) $(RSYS_CFLAGS) -c $(@:.o=.c) -o $@

libsadist_trilinear_profile.so: src/sadist_lib_trilinear_profile.o
	$(CC) $(CFLAGS_SO) $(RSYS_CFLAGS) -o $@ src/sadist_lib_trilinear_profile.o \
	$(LDFLAGS_SO) $(RSYS_LIBS)

src/sadist_lib_unsteady_profile.o:\
 config.mk src/sadist.h src/sadist_lib_unsteady_profile.c
	$(CC) $(CFLAGS_SO) $(RSYS_CFLAGS) -c $(@:.o=.c) -o $@

libsadist_unsteady_profile.so: src/sadist_lib_unsteady_profile.o
	$(CC) $(CFLAGS_SO) $(RSYS_CFLAGS) -o $@ src/sadist_lib_unsteady_profile.o \
	$(LDFLAGS_SO) $(RSYS_LIBS) -lm

################################################################################
# Tests
################################################################################
src/sadist_conducto_radiative.o:\
 config.mk src/sadist.h src/sadist_conducto_radiative.c
	$(CC) $(CFLAGS_EXE) -c $(@:.o=.c) -o $@

sadist_conducto_radiative: config.mk src/sadist_conducto_radiative.o
	$(CC) $(CFLAGS_EXE) -o $@ src/$@.o $(LDFLAGS_EXE) $(RSYS_LIBS)

src/sadist_custom_conductive_path.o:\
 config.mk src/sadist.h src/sadist_custom_conductive_path.c
	$(CC) $(CFLAGS_EXE) $(S3DUT_CFLAGS) $(RSYS_CFLAGS) -c $(@:.o=.c) -o $@

sadist_custom_conductive_path: config.mk src/sadist_custom_conductive_path.o
	$(CC) $(CFLAGS_EXE) $(S3DUT_CFLAGS) $(RSYS_CFLAGS) -o $@ src/$@.o \
	$(LDFLAGS_EXE) $(S3DUT_LIBS) $(RSYS_LIBS)

src/sadist_external_flux.o:\
 config.mk src/sadist.h src/sadist_external_flux.c
	$(CC) $(CFLAGS_EXE) -c $(@:.o=.c) -o $@

sadist_external_flux: config.mk src/sadist_external_flux.o
	$(CC) $(CFLAGS_EXE) -o $@ src/$@.o $(LDFLAGS_EXE) $(RSYS_LIBS) -lm

src/sadist_probe_boundary.o:\
 config.mk src/sadist.h src/sadist_probe_boundary.c
	$(CC) $(CFLAGS_EXE) $(S3DUT_CFLAGS) $(RSYS_CFLAGS) -c $(@:.o=.c) -o $@

sadist_probe_boundary: config.mk src/sadist_probe_boundary.o
	$(CC) $(CFLAGS_EXE) $(S3DUT_CFLAGS) $(RSYS_CFLAGS) -o $@ src/$@.o \
	$(LDFLAGS_EXE) $(S3DUT_LIBS) $(RSYS_LIBS) -lm

src/sadist_unsteady.o:\
 config.mk src/sadist.h src/sadist_unsteady.c
	$(CC) $(CFLAGS_EXE) $(S3DUT_CFLAGS) $(RSYS_CFLAGS) -c $(@:.o=.c) -o $@

sadist_unsteady: config.mk src/sadist_unsteady.o
	$(CC) $(CFLAGS_EXE) $(S3DUT_CFLAGS) $(RSYS_CFLAGS) -o $@ src/$@.o \
	$(LDFLAGS_EXE) $(S3DUT_LIBS) $(RSYS_LIBS) -lm
